# Awesome DevTools [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)


### Code Editors & IDEs

### Text Editors
- Integrated Development Environments (IDEs)
- Code Editor Extensions & Plugins
- Theme & Customization
- Collaboration Tools for Coding
### Version Control & Collaboration

- Git Clients & Tools
- Code Review Platforms
- Collaboration Platforms for Developers
- Project Management Tools for Dev Teams
- Debugging & Testing

### Debuggers
- Testing Frameworks
- Test Runners
- Mocking & Stubbing Libraries
- Performance Profiling Tools
- Build Tools & Task Runners

### Build Tools
- Task Runners & Automation
- Package Managers
- API Development & Testing

### API Design Tools
- API Testing Tools
- API Mocking & Simulation
- API Documentation Generators
- API Gateways & Management
- Front-End Development

### Front-End Frameworks
- Component Libraries
- Styling Tools
- CSS Frameworks
- JavaScript Libraries & Utilities
- Back-End Development

### Back-End Frameworks
- Databases
- Web Servers
- Object-Relational Mappers (ORMs)
- DevOps & Infrastructure

### Infrastructure as Code (IaC) Tools
- Configuration Management Tools
- Deployment Tools
- Containerization Tools
- Cloud Providers & Services
- Security

### Static Code Analysis Tools
- Dynamic Code Analysis Tools
- Security Scanning Tools
- Penetration Testing Tools
- Web Application Firewalls (WAFs)
- Other

## Terminal Emulators & Shell Enhancements
- Productivity Tools for Developers
- Knowledge Bases & Learning Platforms
- Low-Code/No-Code Development Platforms
- Data Visualization Tools
- Monitoring & Observability Tools
- Chatbots & Assistants for Developers
- AI-Powered Coding Assistants
- Browser Extensions for Developers